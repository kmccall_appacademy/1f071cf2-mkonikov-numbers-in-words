class Fixnum

    ONES = {
      1 => 'one',
      2 => 'two',
      3 => 'three',
      4 => 'four',
      5 => 'five',
      6 => 'six',
      7 => 'seven',
      8 => 'eight',
      9 => 'nine',
      0 => 'zero'
    }
    TENS = {
      20 => 'twenty',
      30 => 'thirty',
      40 => 'forty',
      50 => 'fifty',
      60 => 'sixty',
      70 => 'seventy',
      80 => 'eighty',
      90 => 'ninety'
    }
    TEENS = {
      10 => 'ten',
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirteen',
      14 => 'fourteen',
      15 => 'fifteen',
      16 => 'sixteen',
      17 => 'seventeen',
      18 => 'eighteen',
      19 => 'nineteen'
    }
    BEYOND = {
      100 => 'hundred',
      1000 => 'thousand',
      1000000 => 'million',
      1000000000 => 'billion',
      1000000000000 => 'trillion'
    }

  def in_words
    result = []

    if self < 10
    result << ONES[self]
  elsif self < 20
    result << TEENS[self]
  elsif self < 100
    digit1 = (self / 10) * 10
    result << TENS[digit1]
    digit2 = self - digit1
    if digit2 != 0
      result << digit2.in_words
    end
  else
    level = findlevel(self)
    digit1 = self / level
    result << digit1.in_words
    result << BEYOND[level]
    remainder = self
    remainder -= (digit1 * level)
    if remainder != 0
      result << remainder.in_words
    end
  end

  result.join(" ")

  end

  def findlevel(num)
    beyond = {
      100 => 'hundred',
      1000 => 'thousand',
      1000000 => 'million',
      1000000000 => 'billion',
      1000000000000 => 'trillion'
    }
    beyond.select { |k| k <= num }.to_a[-1][0]
  end

end
